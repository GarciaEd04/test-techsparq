const express = require('express')
const path = require('path')
const app = express();

app.use(express.static(path.join(__dirname,"src/public")))

app.get('/', function(req, res, next) {
  res.sendFile(__dirname + '/src/views/index.html');
});

app.listen(3000);

console.log('Server is running at: http://localhost:3000');