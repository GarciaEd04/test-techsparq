# Edwar Garcia Exercise 1

## Installation

Use the package manager [npm](https://nodejs.org/en/) to install the project.

```bash
npm install
```

## Run in local

```bash
npm run serve
```

### Server runs at
```bash
http://localhost:3000/
```

### Run unit tests
```bash
cd src
```

Then open SpecRunner.html into browser