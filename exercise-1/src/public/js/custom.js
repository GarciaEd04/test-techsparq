async function fetchData () {
  try {
    const response = await $.get('https://5dc588200bbd050014fb8ae1.mockapi.io/assessment')
    renderData(response)
    return response;
  } catch (error) {
    renderData(error)
  }
}

function renderData(data) {
  localStorage.setItem('toggle-createdAt', false)
  localStorage.setItem('toggle-id', false)
  if (Array.isArray(data)) {
    const template = Handlebars.compile(`
      <div class="col">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th scope="col" class="d-none toggle-id">Id</th>
              <th scope="col">Avatar</th>
              <th scope="col">Name</th>
              <th scope="col" class="d-none toggle-createdAt">Created Date</th>
            </tr>
          </thead>
          <tbody>
            {{#assessments}}
            <tr>
              <th scope="row" class="d-none toggle-id">{{ id }}</th>
              <td>
                <img src="{{ avatar }}" alt="{{ name }}" class="avatar">
              </td>
              <td>{{ name }}</td>
              <td class="d-none toggle-createdAt">{{ createdAt }}</td>
            </tr>
            {{/assessments}}
          </tbody>
        </table>
      </div>
    `)
    const assessments = data.map(((assessment) => {
      return { ...assessment, createdAt: new Date(assessment.createdAt).toLocaleDateString() }
    }))
    $("#content-placeholder").html(template({ assessments }));
    return true
  } else {
    $('#error-alert').removeClass('d-none');
    return false
  }
}

function handleToggleInfo(type) {
  const flag = localStorage.getItem(type) === 'false'
  let message = ''
  switch (type) {
    case 'toggle-id':
      message = flag ? 'Hide Id' : 'Show Id'
      break;
    case 'toggle-createdAt':
      message = flag ? 'Hide creation date' : 'Show creation date'
      break;
    default:
      break;
  }
  $(`#${type}`).text(message)
  localStorage.setItem(type, flag)
  const classToSet = flag ? type : `d-none ${type}`
  $(`.${type}`).attr('class', classToSet)
  return { message, type }
}

fetchData()