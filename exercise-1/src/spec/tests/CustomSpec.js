const mockAssessments = [
  { "id": "16", "createdAt": "2021-03-31T13:24:14.020Z","name": "Ryann Wiegand", "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/daykiine/128.jpg" },
  { "id": "17", "createdAt": "2021-03-31T14:36:34.443Z", "name": "Waldo Weimann", "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/dotgridline/128.jpg" },
  { "id": "18", "createdAt": "2021-03-31T11:00:08.520Z", "name": "Reed Pouros", "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/salvafc/128.jpg" }
]

describe("Testing main functionalities", () => {
  it("Must return assessments data", async () => {
    const response = await fetchData();
    expect(Array.isArray(response)).toBe(true)
  })

  it("Must render assessments data", () => {
    const response = renderData(mockAssessments)
    expect(response).toBe(true)
  })
  
  it("Must render error message", () => {
    const response = renderData('error')
    expect(response).toBe(false)
  })

  it("Must toggle id", () => {
    const response = handleToggleInfo('toggle-id')
    const flag = localStorage.getItem('toggle-id') === 'false'
    expect(response).toEqual({
      message: flag ? 'Show Id' : 'Hide Id',
      type: 'toggle-id'
    })
  })
  
  it("Must toggle created dates", () => {
    const flag = localStorage.getItem('toggle-createdAt') === 'false'
    const response = handleToggleInfo('toggle-createdAt')
    expect(response).toEqual({
      message: flag ? 'Hide creation date' : 'Show creation date',
      type: 'toggle-createdAt'
    })
  })
})