
const taskInput = document.getElementById("new-task");
const addButton = document.getElementsByTagName("button")[0];
const incompleteTasksHolder = document.getElementById("incomplete-tasks");
const completedTasksHolder = document.getElementById("completed-tasks");
let todoTasks = JSON.parse(localStorage.getItem('todoTasks')) || []
let completedTasks = JSON.parse(localStorage.getItem('completedTasks')) || []

const createNewTaskElement = function (taskString) {
  const listItem = document.createElement("li");
  const checkBox = document.createElement("input");
  const label = document.createElement("label");
  const editInput = document.createElement("input");
  const editButton = document.createElement("button");
  const deleteButton = document.createElement("button");

  checkBox.type = "checkbox";
  editInput.type = "text";
  editButton.innerText = "Edit";
  editButton.className = "edit";
  deleteButton.innerText = "Delete";
  deleteButton.className = "delete";
  label.innerText = taskString;

  listItem.appendChild(checkBox);
  listItem.appendChild(label);
  listItem.appendChild(editInput);
  listItem.appendChild(editButton);
  listItem.appendChild(deleteButton);

  return listItem;
};

const addTask = function (taskText) {
  const listItemName = taskInput.value || taskText
  if (typeof listItemName !== "string" || !listItemName) {
    alert('Item name can not be null')
    return
  }
  const listItem = createNewTaskElement(listItemName)
  incompleteTasksHolder.appendChild(listItem)
  bindTaskEvents(listItem, taskCompleted, 'todoTasks')
  taskInput.value = "";
  const todoTasksInMemory = JSON.parse(localStorage.getItem('todoTasks')) || []
  localStorage.setItem('todoTasks', JSON.stringify([...todoTasksInMemory, listItemName]))
};

const editTask = function () {
  const listItem = this.parentNode;
  const editInput = listItem.querySelectorAll("input[type=text]")[0];
  const label = listItem.querySelector("label");

  const button = listItem.getElementsByTagName("button")[0];
  const containsClass = listItem.classList.contains("editMode");
  if (containsClass) {
    const text = listItem.querySelectorAll("label")[0].innerText
    if (typeof editInput.value !== "string" || !editInput.value) {
      alert('Item name can not be null')
      return
    }
    label.innerText = editInput.value
    button.innerText = "Edit";
    const updateButton = listItem.querySelectorAll("button.delete")[0]
    const key = updateButton.dataset.dataType
    const itemsToUpdate = JSON.parse(localStorage.getItem(key))
    const itemsUpdated = itemsToUpdate.map((item) => {
      return item === text ? editInput.value : item;
    })
    localStorage.setItem(key, JSON.stringify(itemsUpdated))
  } else {
    editInput.value = label.innerText
    button.innerText = "Save";
  }

  listItem.classList.toggle("editMode");
};

const deleteTask = function () {
  const listItem = this.parentNode;
  const ul = listItem.parentNode;
  ul.removeChild(listItem);
  const text = listItem.querySelectorAll("label")[0].innerText
  const button = listItem.querySelectorAll("button.delete")[0]
  const key = button.dataset.dataType
  const itemsFiltered = (JSON.parse(localStorage.getItem(key))).filter((item) => item !== text)
  localStorage.setItem(key, JSON.stringify(itemsFiltered))
};

const handleToggleItem = function (key, text, oppositeKey) {
  const itemsFiltered = (JSON.parse(localStorage.getItem(key))).filter((item) => item !== text)
  localStorage.setItem(key, JSON.stringify(itemsFiltered))
  const itemsToAdd = JSON.parse(localStorage.getItem(oppositeKey)) || []
  localStorage.setItem(oppositeKey, JSON.stringify([...itemsToAdd, text]))
}

const taskCompleted = function () {
  const listItem = this.parentNode;
  completedTasksHolder.appendChild(listItem);
  bindTaskEvents(listItem, taskIncomplete, 'completedTasks');
  handleToggleItem('todoTasks', listItem.querySelectorAll("label")[0].innerText, 'completedTasks')
};

const taskIncomplete = function () {
  const listItem = this.parentNode;
  incompleteTasksHolder.appendChild(listItem);
  bindTaskEvents(listItem, taskCompleted, 'todoTasks');
  handleToggleItem('completedTasks', listItem.querySelectorAll("label")[0].innerText, 'todoTasks')
};

const bindTaskEvents = function (taskListItem, checkBoxEventHandler, type, isCompleted = false) {
  const checkBox = taskListItem.querySelectorAll("input[type=checkbox]")[0];
  const editButton = taskListItem.querySelectorAll("button.edit")[0];
  const deleteButton = taskListItem.querySelectorAll("button.delete")[0];
  editButton.dataset.dataType = type
  deleteButton.dataset.dataType = type
  editButton.onclick = editTask;
  deleteButton.onclick = deleteTask;
  checkBox.onchange = checkBoxEventHandler;
  if (isCompleted) {
    checkBox.checked = isCompleted
  }
};

addButton.addEventListener("click", addTask);

for (let i = 0; i < incompleteTasksHolder.children.length; i++) {
  bindTaskEvents(incompleteTasksHolder.children[i], taskCompleted, 'todoTasks');
}

for (let i = 0; i < completedTasksHolder.children.length; i++) {
  bindTaskEvents(completedTasksHolder.children[i], taskIncomplete, 'completedTasks');
}

for (const todoTask of todoTasks) {
  const listItem = createNewTaskElement(todoTask)
  incompleteTasksHolder.appendChild(listItem);
  bindTaskEvents(listItem, taskCompleted, 'todoTasks');
}

for (const completedTask of completedTasks) {
  const listItem = createNewTaskElement(completedTask)
  completedTasksHolder.appendChild(listItem);
  bindTaskEvents(listItem, taskIncomplete, 'completedTasks', true);
}